#include <cstdlib>
#include <iostream>

#include <gtkmm.h>
#include <webkit2/webkit2.h>

Gtk::ApplicationWindow *pApplicationWindow = nullptr;
Glib::RefPtr<Gtk::Application> app;

int main(int argc, char *argv[]) {

  app = Gtk::Application::create("codeberg.speedymouser");
  auto refBuilder = Gtk::Builder::create();
  try {
    refBuilder->add_from_file("ui/app.ui");
  } catch (const Glib::FileError &ex) {
    std::cerr << "FileError: " << ex.what() << std::endl;
    return -1;
  } catch (const Glib::MarkupError &ex) {
    std::cerr << "MarkupError: " << ex.what() << std::endl;
    return -1;
  } catch (const Gtk::BuilderError &ex) {
    std::cerr << "BuilderError: " << ex.what() << std::endl;
    return -1;
  }

  refBuilder->get_widget<Gtk::ApplicationWindow>("Window", pApplicationWindow);
  if (!pApplicationWindow) {
    std::cerr << "Could not get the applicationWindow" << std::endl;
    return -1;
  }
  pApplicationWindow->show();

  Gtk::Box *b = nullptr;
  refBuilder->get_widget<Gtk::Box>("WebBox", b);

  WebKitWebView *webView = WEBKIT_WEB_VIEW(webkit_web_view_new());
  webkit_web_view_load_uri(webView, "https://anoxinon.de");

  Gtk::Widget *w = Glib::wrap(GTK_WIDGET(webView));
  w->show();
  w->set_hexpand(true);
  w->set_vexpand(true);
  b->add(*w);

  return app->run(*pApplicationWindow, argc, argv);
}
